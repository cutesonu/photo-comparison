import os
from flask import Flask, render_template, request, jsonify  # , redirect, url_for, send_from_directory, send_file
from werkzeug.utils import secure_filename


from src.classify import Classify
# from endpoint import calc_similarity
from utils.common import allowed_img_file, allowed_ref_file, clear_folder
import utils.logger as log
from utils.constant import UPLOAD_DIR, KEY_STATE, KEY_MESSAGE, KEY_LABEL, KEY_SCORE
from src.features import validate_ref_data, collect_features
from src.train import train_comparison_model, check_precision


# Init the flask application
app = Flask(__name__)
classifier = Classify()


def build_response(url1, url2, pred_lbl, pred_score):
    """
    :param url1:
    :param url2:
    :param pred_lbl:
    :param pred_score
    :return:
        {
            'baseImage':
                'https://s3.amazonaws.com/era-images/produto/5b3ea0e7ecbdea1bb858cead/media/5b58bea6ecbdea0ef4a70293.jpg'
            'comparisonImage':
                'https://s3.amazonaws.com/intellibrand-data-mining/RawCrawledData/8f5d3229-41ac-11e8-9fae-d75352c2fcbf/BR/2018/09/20/07a01968-bc92-11e8-aec0-af6032242b39/img/90c63617946a24967eae9bb104594110.jpg'
            'percentage': 57
        }

    """

    percent = round(pred_score * 100)

    return {
        'baseImage': url1,
        'comparisonImage': url2,
        'result': pred_lbl,
        'percentage': percent,
    }


@app.route('/')
def prog():
    return render_template('index.html')


@app.route('/compare_url', methods=['POST'])
def compare_url():
    if request.method == 'POST':
        url1 = request.form['url1']
        url2 = request.form['url2']
        log.info("{}".format(request))

        if url1 and url2:
            try:
                if not allowed_img_file(url1):
                    msg = 'No allowed file format BaseImage {}'.format(url1)
                    log.info(msg)
                    return msg
                if not allowed_img_file(url2):
                    msg = 'No allowed file format ComparisonImage {}'.format(url2)
                    log.info(msg)
                    return msg

                ret = classifier.proc(url1=url1, url2=url2)
                if not ret[KEY_STATE]:
                    return str(ret[KEY_MESSAGE])
                else:
                    result_dict = build_response(url1=url1, url2=url2, pred_lbl=ret[KEY_LABEL],
                                                 pred_score=ret[KEY_SCORE])
                    return jsonify(result_dict)

            except Exception as e:
                log.error(str(e))
                return str(e)

    else:
        msg = 'No post data found'
        log.info(msg + "\n")
        return msg


@app.route('/compare_buffer', methods=['POST'])
def compare_buffer():
    # Support uploading files, just in case
    if len(request.files) > 0:
        file1 = request.files['file1']
        file2 = request.files['file2']

        log.info("{}".format(request))

        if file1 and file2:
            try:
                img_fn1 = secure_filename(file1.filename)
                img_fn2 = secure_filename(file2.filename)

                if not allowed_img_file(file1.filename):
                    msg = 'No allowed file format BaseImage {}'.format(img_fn1)
                    log.info(msg)
                    return msg

                if not allowed_img_file(file2.filename):
                    msg = 'No allowed file format ComparisonImage {}'.format(img_fn2)
                    log.info(msg)
                    return msg

                log.info("uploading the image file")
                # remove all previous json files in uploader directory
                if not os.path.exists(UPLOAD_DIR):
                    os.mkdir(UPLOAD_DIR)
                else:
                    clear_folder(folder=UPLOAD_DIR)

                # save image file on Upload_dir
                save_path1 = os.path.join(UPLOAD_DIR, img_fn1)
                log.info("Save the image1 -> {}".format(save_path1))
                file1.save(save_path1)

                save_path2 = os.path.join(UPLOAD_DIR, img_fn2)
                log.info("Save the image2 -> {}".format(save_path2))
                file2.save(save_path2)

                # result = calc_similarity(url1=save_path1, url2=save_path2)
                # result_dict = {"similarity": "{} %".format(round(result * 100, 2))}

                ret = classifier.proc(url1=save_path1, url2=save_path2)
                if not ret[KEY_STATE]:
                    return str(ret[KEY_MESSAGE])
                else:
                    result_dict = build_response(url1=save_path1, url2=save_path2, pred_lbl=ret[KEY_LABEL],
                                                 pred_score=ret[KEY_SCORE])

                    return jsonify(result_dict)

            except Exception as e:
                log.error(str(e))
                return str(e)
    else:
        msg = 'No file or post data found'
        log.info(msg + "\n")
        return msg


@app.route('/train_model', methods=['POST'])
def train_model():
    # Support uploading files, just in case
    if len(request.files) > 0:
        file = request.files['file']

        log.info("{}".format(request))

        if file:
            try:
                fn = secure_filename(file.filename)

                if not allowed_ref_file(fn):
                    msg = 'No allowed file format BaseImage {}'.format(fn)
                    log.info(msg)
                    return msg

                log.info("uploading the train data csv file")
                # remove all previous json files in uploader directory
                if not os.path.exists(UPLOAD_DIR):
                    os.mkdir(UPLOAD_DIR)
                else:
                    clear_folder(folder=UPLOAD_DIR)

                # save image file on Upload_dir
                save_path = os.path.join(UPLOAD_DIR, fn)
                log.info("Save the ref file -> {}".format(save_path))
                file.save(save_path)

                # result = calc_similarity(url1=save_path1, url2=save_path2)
                # result_dict = {"similarity": "{} %".format(round(result * 100, 2))}

                ret = validate_ref_data(ref_path=save_path)
                if isinstance(ret, str):  # error message
                    return str(ret)
                else:
                    if collect_features():
                        if train_comparison_model():
                            check_precision()
                            return "Successfully updated the comparison model"
                        else:
                            return "Failed to training the model"
                    else:
                        return "Failed to collect the features"

            except Exception as e:
                log.error(str(e))
                return str(e)
    else:
        msg = 'No file or post data found'
        log.info(msg + "\n")
        return msg


if __name__ == '__main__':

    port = int(os.environ.get('PORT', 5000))

    app.run(
        host='0.0.0.0',
        port=port,
        threaded=True,
        debug=False
    )
