from src.image_object import ImageObject
from src.train import load_classifier_model, predict
from utils.constant import KEY_SCORE, KEY_LABEL, KEY_STATE, KEY_MESSAGE


class Classify:
    def __init__(self):
        self.classifier = load_classifier_model()
        self.im_ob = ImageObject()

    def proc(self, url1, url2):
        # Extract the feature vector per each image

        ret = {
            KEY_STATE: False,
            KEY_MESSAGE: '',
            KEY_LABEL: '',
            KEY_SCORE: ''
        }

        try:
            ret1, img_obj1 = self.im_ob.init(source=url1)
            ret2, img_obj2 = self.im_ob.init(source=url2)

            if not ret1:
                ret[KEY_STATE] = False
                ret[KEY_MESSAGE] = "failed to load image1 {}".format(url1)

            if not ret2:
                ret['state'] = False
                ret[KEY_MESSAGE] = "failed to load image1 {}".format(url2)

            features = [img_obj1['feature'], img_obj2['feature']]
            pred_lbl, pred_score = predict(classifier=self.classifier, features=features)

            print("predict result: ")
            print("\timage url1: {}".format(url1))
            print("\timage url2: {}".format(url2))
            print("\tlabel: {}, confidence: {}".format(pred_lbl, round(pred_score, 2)))

            ret[KEY_STATE] = True
            ret[KEY_MESSAGE] = 'successfully processed'
            ret[KEY_LABEL] = pred_lbl
            ret[KEY_SCORE] = pred_score

        except Exception as e:
            ret[KEY_STATE] = False
            ret[KEY_MESSAGE] = str(e)

        return ret
