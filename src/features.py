import sys
import os
import csv
import cv2
import pandas as pd

from src.image_object import ImageObject
from src.train import predict, load_classifier_model
from utils.constant import FEATURES_DIR, RAW_DIR, LBL_CORRECT, LBL_INCORRECT
from utils.common import clear_string, check_local_exist

im_ob = ImageObject()

TXT_TRAIN_DATA_FILE = os.path.join(FEATURES_DIR, "train_data.txt")


def __load_reference_xlsx(xlsx_path, txt_path, sheet_name=0):

    try:
        df = pd.read_excel(xlsx_path, sheet_name=sheet_name)

        base_urls = df['ManufacturerUrl'].tolist()  # ManufacturerUrl, Base
        compare_urls = df['RetailerUrl'].tolist()  # RetailerUrl, Compare
        labels = df['Human'].tolist()  # Human, Label

        with open(txt_path, 'w') as fp:
            for idx in range(len(base_urls)):
                line = "{}, {}, {}\n".format(base_urls[idx], compare_urls[idx], labels[idx])
                fp.write(line)

        return True

    except Exception as e:
        print(str(e))
        return str(e)


def validate_ref_data(ref_path):
    txt_train_data_file = TXT_TRAIN_DATA_FILE

    ret = __load_reference_xlsx(xlsx_path=ref_path, txt_path=txt_train_data_file)
    return ret


def collect_features():
    txt_train_data_file = TXT_TRAIN_DATA_FILE
    # --------- check the raw image pairs ----------------------------------------------
    print('>>> check the raw image pairs')
    if not os.path.exists(txt_train_data_file):
        print(" not exist source urls")
        sys.exit(1)

    url_pairs = []
    labels = []
    classes = []
    with open(txt_train_data_file, 'r') as f:
        lines = f.readlines()
        for line in lines:
            sps = line.split(',')
            if len(sps) < 3:
                continue

            url1 = clear_string(sps[0])
            url2 = clear_string(sps[1])
            label = clear_string(sps[2]).replace('\n', '').lower()

            if len(label) == 0:
                continue

            if label.lower()[0] in ["t", "1"]:
                label = LBL_CORRECT
            elif label.lower()[0] in ["f", "0"]:
                label = LBL_INCORRECT
            else:
                continue

            labels.append(label)

            if label not in classes:
                classes.append(label)

            url_pairs.append([url1, url2])

    print(" len pairs: ", len(url_pairs))
    classes.sort()
    print(" classes: ", classes)

    # --- check the raw images ----------------------------------------------------------
    counts = ([0] * len(classes)).copy()
    features = []
    local_paths = []
    filtered_labels = []

    print('>>> collect train data(features) from the raw urls')
    if not os.path.isdir(RAW_DIR):
        print(" not exist folder for raw image data")
        return

    for idx, label in enumerate(labels):
        print(idx, label)
        url1, url2 = url_pairs[idx]

        url1 = check_local_exist(url1)
        ret1, img_obj1 = im_ob.init(source=url1)

        url2 = check_local_exist(url2)
        ret2, img_obj2 = im_ob.init(source=url2)

        if not ret1 or not ret2:
            print(' failed to collect features from index {} {}, {}'.format(idx, url1, url2))
            continue

        # diff_feature = img_obj1['feature'] - img_obj2['feature']
        line = img_obj1['feature'].tolist() + img_obj2['feature'].tolist()
        features.append(line)

        local_paths.append("{}\t{}".format(img_obj1['local_path'], img_obj2['local_path']))

        filtered_labels.append(label)

        counts[classes.index(label)] += 1

    for idx, class_name in enumerate(classes):
        print(" class: {}, counts #: {}".format(class_name, counts[idx]))

    # --- write the train_data.csv file on the same location --------------------------------------
    print(">>> save train features and labels")
    save_dir = FEATURES_DIR
    if not os.path.exists(save_dir):
        os.mkdir(save_dir)

    with open(os.path.join(save_dir, "train_data.csv"), 'w', newline='') as fp:
        wr = csv.writer(fp, delimiter=',')
        wr.writerows(features)
    print(" create the train_data.csv successfully!")

    # write the train_classes.txt on the same location
    with open(os.path.join(save_dir, "train_classes.txt"), 'w') as fp:
        for class_name in classes:
            fp.write(class_name + "\n")
    print(" create the train_classes.txt successfully!")

    # write the train_labels.txt on the same location
    with open(os.path.join(save_dir, "label_list.txt"), 'w') as fp:
        for label in filtered_labels:
            fp.write(label + "\n")
    print(" create the label_list.txt successfully!")

    # write the local_path_list.txt on the same location
    with open(os.path.join(save_dir, "local_path_list.txt"), 'w') as fp:
        for local_path in local_paths:
            fp.write(local_path + "\n")
    print(" create the local_path.txt successfully!")
    return True


def check_images_with_label():
    classifier = load_classifier_model()
    with open(os.path.join(FEATURES_DIR, "local_path_list.txt"), 'r') as f:
        str_lines = f.readlines()
    with open(os.path.join(FEATURES_DIR, "label_list.txt"), 'r') as f:
        labels = f.readlines()

    for idx, line in enumerate(str_lines):
        paths = line.replace('\n', '').split('\t')
        label = labels[idx]
        img1 = cv2.imread(paths[0])
        img2 = cv2.imread(paths[1])

        ret1, img_obj1 = im_ob.init(source=paths[0])
        ret2, img_obj2 = im_ob.init(source=paths[1])
        if not ret1 or not ret2:
            print(' failed to collect features from index {} {}, {}'.format(idx, paths[0], paths[1]))
            continue

        # diff_feature = img_obj1['feature'] - img_obj2['feature']
        pred_lbl = predict(classifier=classifier, features=[img_obj1['feature'].tolist(), img_obj2['feature'].tolist()])

        cv2.imshow("img1", img1)
        cv2.imshow("img2", img2)
        print(paths[0], paths[1])
        print(label, pred_lbl)
        cv2.waitKey(0)
