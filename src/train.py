import sys
import os
import csv
import numpy as np
from sklearn.svm import SVC
from sklearn.externals import joblib


from src.image_object import ImageObject
from utils.constant import FEATURES_DIR, CLASSIFIER_DIR


im_oj = ImageObject(b_normalize=True, b_equalize=False)


def load_feature_and_label():
    print('load feature and labels')

    feature_data_path = os.path.join(FEATURES_DIR, "train_data.csv")
    feature_label_path = os.path.join(FEATURES_DIR, "label_list.txt")
    classes_path = os.path.join(FEATURES_DIR, "train_classes.txt")

    if not os.path.exists(feature_data_path):
        print(" not exist train feature data {}".format(feature_data_path))
        sys.exit(0)
    if not os.path.exists(feature_label_path):
        print(" not exist train label data {}".format(feature_label_path))
        sys.exit(0)
    if not os.path.exists(classes_path):
        print(" not exist classes {}".format(classes_path))
        sys.exit(0)

    class_names = []
    # --- loading class names ---------------------------------------------------------------
    print(' loading training class_names ... ')
    with open(classes_path, 'r') as fp:
        for line in fp:
            line = line.replace('\n', '')
            class_names.append(line)

    data = []
    features = []
    # --- loading data -----------------------------------------------------------------------
    print(' loading training features ... ')
    with open(feature_data_path) as fp:
        csv_reader = csv.reader(fp, delimiter=',')
        for row in csv_reader:
            _line = [float(row[i]) for i in range(0, len(row))]

            len_feature = int(len(_line)) // 2
            _feature1 = _line[:len_feature]
            _feature2 = _line[len_feature: 2 * len_feature]

            features.append([_feature1, _feature2])
            data.append(np.array(_feature1) - np.array(_feature2))

    labels = []
    # --- loading labels ---------------------------------------------------------------------
    print(' loading labels ... ')
    with open(feature_label_path, 'r') as fp:
        for line in fp:
            label = line.replace('\n', '')
            labels.append(label)

    return data, labels, class_names, features


def train_comparison_model():
    print('>>> train')
    classifier_path = os.path.join(CLASSIFIER_DIR, "classifier.pkl")
    if not os.path.exists(CLASSIFIER_DIR):
        os.mkdir(CLASSIFIER_DIR)

    # --- load feature and label data ----------------------------------------------------------------
    data, labels, label_names, features = load_feature_and_label()

    # --- training -----------------------------------------------------------------------------------
    print(' training... ')
    # classifier = SVC(C=1.0, kernel='linear', degree=3, gamma='auto', coef0=0.0, shrinking=True, probability=True,
    #                  tol=0.001, cache_size=200, class_weight='balanced', verbose=False, max_iter=-1,
    #                  decision_function_shape='ovr', random_state=None)
    classifier = SVC(kernel='rbf', degree=3, gamma='scale',
                     coef0=0.0, tol=1e-3, C=1.0, probability=True,
                     cache_size=100, max_iter=-1)

    classifier.fit(data, labels)
    joblib.dump(classifier, classifier_path)

    print('>>> finished the training!')
    return True


def load_classifier_model():
    classifier_path = os.path.join(CLASSIFIER_DIR, "classifier.pkl")

    if not os.path.exists(classifier_path):
        print(" not exist trained classifier {}".format(classifier_path))
        sys.exit(0)

    try:
        # loading
        model = joblib.load(classifier_path)
        return model
    except Exception as ex:
        print(ex)
        sys.exit(1)


def predict(classifier, features):
    diff_feature = np.array(features[0]) - np.array(features[1])
    diff_feature.tolist()

    d1 = np.linalg.norm(np.array(features[0]))
    d2 = np.linalg.norm(np.array(features[1]))
    dist = np.linalg.norm(diff_feature)

    feature = diff_feature.reshape(1, -1)
    # Get a prediction from the imgnet including probability:
    probab = classifier.predict_proba(feature)

    max_ind = np.argmax(probab)
    sort_probab = np.sort(probab, axis=None)[::-1]  # Rearrange by size

    pred_lbl = classifier.classes_[max_ind]
    pred_score = sort_probab[0]

    print(d1, d2, dist, pred_lbl, pred_score)
    return pred_lbl, pred_score


def check_precision():
    # --- load trained classifier imgnet --------------------------------------------------------------
    print('>>> checking the precision...')
    classifier = load_classifier_model()

    # --- load feature and label data ----------------------------------------------------------------
    data, labels, label_names, features_list = load_feature_and_label()

    confusion_matrix = np.zeros((len(label_names), len(label_names)), dtype=np.int)

    positive, negative = 0, 0
    # --- check confuse matrix ------------------------------------------------------------------------
    for i in range(len(data)):
        features = features_list[i]

        print(">>>GT:", labels[i])
        predlbl, score = predict(classifier=classifier, features=features)

        # update confusion matrix
        predlbl_idx = label_names.index(predlbl)
        ground_truth_idx = label_names.index(labels[i])

        confusion_matrix[predlbl_idx][ground_truth_idx] += 1
        if predlbl_idx == ground_truth_idx:
            positive += 1
            print("\t\tPositive")
        else:
            negative += 1
            print("\t\tNegative")

    print('>>> precision result')
    print(confusion_matrix)

    precision = positive / (positive + negative)
    print("\tprecision : {} / {} : {}%".format(positive, positive + negative, round(precision * 100, 2)))


def test(url1, url2):
    # Extract the feature vector per each image
    classifier = load_classifier_model()

    ret, img_obj1 = im_oj.init(source=url1)
    ret, img_obj2 = im_oj.init(source=url2)

    features = [img_obj1['feature'], img_obj2['feature']]

    pred_lbl, pred_score = predict(classifier=classifier, features=features)

    print("predict result: ")
    print("\timage url1: {}".format(url1))
    print("\timage url2: {}".format(url2))
    print("\tlabel: {}, confidence: {}".format(pred_lbl, round(pred_score, 2)))
    return pred_lbl, pred_score
