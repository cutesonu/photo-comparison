import os
import numpy as np
import cv2
from PIL import Image
import requests
from io import BytesIO


from utils.constant import RAW_DIR
from src.imgnet import ImgNet

from utils.constant import NORMAL_WIDTH, FEATURE_EMBED
img_net = ImgNet(mode=FEATURE_EMBED)


# from utils.constant import FEATURE_LOOKUP
# img_net = ImgNet(mode=FEATURE_LOOKUP)


class ImageObject:
    def __init__(self, b_normalize=True, b_equalize=False):
        self.b_normalize = b_normalize
        self.b_equalize = b_equalize

    def init(self, source):
        obj = {
            'source': source,
            'image': None,
            'local_path': None,
            'feature': None,
            'message': None
        }

        img, ret = self.load_image(source=source)
        if img is None:
            obj['message'] = ret
            return None, obj
        else:
            local_path = ret
            msg = 'success'

        if self.b_normalize:
            img = self.normalize(img=img)
        if self.b_equalize:
            img = self.equalize(img=img)

        feature = img_net.get_feature_from_cv_mat(cvimg=img)

        obj['image'] = img
        obj['local_path'] = local_path
        obj['feature'] = feature
        obj['message'] = msg

        if img is None:
            return False, obj
        else:
            return True, obj

    @staticmethod
    def load_image(source):
        try:
            if source.find("://") != -1:
                response = requests.get(source)
                pil_img = Image.open(BytesIO(response.content))
            else:
                pil_img = Image.open(source, 'r')

            if pil_img is None:
                err_msg = "failed to load image file [{}]".format(source)
                return None, err_msg

            cv_img = np.array(pil_img)

            # check the channel mode
            if pil_img.mode == 'RGBA':
                cv_img = cv2.cvtColor(cv_img, cv2.COLOR_RGBA2BGR)
            elif pil_img.mode == 'RGB':
                cv_img = cv2.cvtColor(cv_img, cv2.COLOR_RGB2BGR)
            elif pil_img.mode == 'L':
                cv_img = cv2.cvtColor(cv_img, cv2.COLOR_GRAY2BGR)
            else:
                err_msg = "Unknown mode: {}".format(pil_img.mode)
                return None, err_msg

            save_path = os.path.join(RAW_DIR, os.path.split(source)[1])
            if not os.path.exists(save_path):
                cv2.imwrite(save_path, cv_img)

            return cv_img, save_path

        except Exception as e:
            return None, str(e)

    @staticmethod
    def normalize(img):
        if img is None:
            return None
        else:
            h, w = img.shape[:2]
            zoom_factor = NORMAL_WIDTH * 1.0 / w
            img = cv2.resize(img, None, fx=zoom_factor, fy=zoom_factor)
            return img

    @staticmethod
    def equalize(img):
        yuv = cv2.cvtColor(img, cv2.COLOR_BGR2YUV)
        yuv[:, :, 0] = cv2.equalizeHist(yuv[:, :, 0])
        equ = cv2.cvtColor(yuv, cv2.COLOR_YUV2BGR)
        return equ


if __name__ == '__main__':
    path = os.path.join(RAW_DIR, "494e4dbbb425063ca9d629d72d234b80.webp")
    _img = cv2.imread(path)

    cv2.imshow("img", _img)
    cv2.waitKey(0)
