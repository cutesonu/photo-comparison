import os

# --------------------------------------- [PATHS] ----------------------------------------------------------------------
_cur_dir = os.path.dirname(os.path.realpath(__file__))
# ROOT
ROOT_DIR = os.path.join(_cur_dir, os.pardir)

# source
SRC_DIR = os.path.join(ROOT_DIR, 'src')
DATA_DIR = os.path.join(ROOT_DIR, 'data')
RAW_DIR = os.path.join(DATA_DIR, 'raw')
TRAIN_DATA_DIR = os.path.join(DATA_DIR, 'train')
FEATURES_DIR = os.path.join(DATA_DIR, 'features')

# models
MODEL_DIR = os.path.join(ROOT_DIR, "model")
IMGNET_DIR = os.path.join(MODEL_DIR, "imgnet")
CLASSIFIER_DIR = os.path.join(MODEL_DIR, "classifier")

# logs
LOG_DIR = os.path.join(ROOT_DIR, 'logs')

UPLOAD_DIR = os.path.join(DATA_DIR, 'uploads')

# --------------------------------------- [INPUT DATA] -----------------------------------------------------------------
IMG_EXTs = [".jpg", ".jpeg", ".png", ".tif", ".bmp", ".webp", ".gif"]  # allowed image extensions

REF_EXTs = ['.xlsx']

# --------------------------------------- [CLASSIFICATION] -------------------------------------------------------------
FEATURE_EMBED = "embed"
FEATURE_LOOKUP = "lookup"

LABELS = ["True", "False"]

NORMAL_WIDTH = 500


KEY_STATE = 'state'
KEY_LABEL = 'label'
KEY_SCORE = 'score'
KEY_MESSAGE = 'message'

LBL_CORRECT = 'true'
LBL_INCORRECT = 'false'


BINARY_THRESH = 0.65
