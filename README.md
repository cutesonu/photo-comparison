# Photo-Comparison


## API Structure
url:        http://ec2-34-203-38-68.compute-1.amazonaws.com:5000/
endpoint:   compare_url
method:     POST

formdata/body
key: value
url1: https://s3.amazonaws.com/era-images/produto/56b3881c99f6d44f1c4928ff/media/5c6459d45da8b645e46f545f.jpg
key: value
url2: https://s3.amazonaws.com/intellibrand-data-mining/RawCrawledData/8f5d324c-41ac-11e8-9fae-d75352c2fcbf/BR/2019/03/26/43649bdc-4f6c-11e9-98a8-5307b667d206/img/0c542deb5becd5aebb9104d532b1ce33.jpg


{
    "baseImage": "https://s3.amazonaws.com/era-images/produto/56b3881c99f6d44f1c4928ff/media/5c6459d45da8b645e46f545f.jpg",
    "comparisonImage": "https://s3.amazonaws.com/intellibrand-data-mining/RawCrawledData/8f5d324c-41ac-11e8-9fae-d75352c2fcbf/BR/2019/03/26/43649bdc-4f6c-11e9-98a8-5307b667d206/img/0c542deb5becd5aebb9104d532b1ce33.jpg",
    "percentage": "92.0%"
}

## Steps

#### Generate the reference.csv

title:

            ManufacturerUrl	|   RetailerUrl |   Label

#### Create the train data from the reference data.
        
        python3 features.py data/ref_05_16_2.txt
    
create the train_data.csv
create the train_classes.txt
create the label_list.txt
create the local_path_list.txt

    class: false, counts #: 468
    class: true, counts #: 416
 
#### Train the model
        
        python3 train.py
        
upload the model `model/classifier/classifier.pkl` 

#### Rerun the Server
        
        python3 app.py
