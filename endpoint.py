import os
import numpy as np
from utils.constant import KEY_SCORE, KEY_LABEL, KEY_STATE, KEY_MESSAGE


from src.image_object import ImageObject
from utils.common import clear_string, check_local_exist


im_oj = ImageObject(b_normalize=True, b_equalize=False)


def calc_similarity(url1, url2):
    ret = {
        KEY_STATE: False,
        KEY_MESSAGE: '',
        KEY_LABEL: '',
        KEY_SCORE: ''
    }
    try:
        print("------------------------------------------------------------------------------------")
        print("")
        print("source 1: {}".format(url1))
        state, img_obj1 = im_oj.init(source=url1)
        if not state:
            print(img_obj1['message'])

            ret[KEY_STATE] = state
            ret[KEY_MESSAGE] = img_obj1['message']
            return ret
        print("\tsize: ", img_obj1['image'].shape[:2])
        print("\tsaved path: ", os.path.split(img_obj1['local_path'])[1])

        print("source 2: {}".format(url2))
        state, img_obj2 = im_oj.init(source=url2)
        if not state:
            print(img_obj2['message'])

            ret[KEY_STATE] = state
            ret[KEY_MESSAGE] = img_obj2['message']
            return ret
        print("\tsize: ", img_obj2['image'].shape[:2])
        print("\tsaved path: ", os.path.split(img_obj2['local_path'])[1])

        print("")
        vec1 = img_obj1['feature']
        vec2 = img_obj2['feature']
        len1 = np.sqrt(np.sum(vec1 ** 2))
        len2 = np.sqrt(np.sum(vec2 ** 2))
        len_diff = np.sqrt(np.sum((vec1 - vec2) ** 2))

        print(len1, len2, len_diff, len_diff * 2.0 / (len1 + len2), len_diff / len1)

        ret[KEY_STATE] = True
        ret[KEY_MESSAGE] = 'successfully processed'
        score = 1 - (len_diff * 2.0 / (len1 + len2))
        if score > 0.5:
            lbl = 'Correct'
        else:
            lbl = 'Incorrect'

        ret[KEY_LABEL] = lbl
        ret[KEY_SCORE] = score

        # cv2.imshow("img1", img_obj1['image'])
        # cv2.imshow("img2", img_obj2['image'])
        # cv2.waitKey(0)
    except Exception as e:
        ret[KEY_STATE] = False,
        ret[KEY_MESSAGE] = str(e)

    return ret


def batch_process(urls_path):
    results = []
    with open(urls_path, 'r') as f:
        lines = f.readlines()
        for line in lines:
            urls = line.split('\t')

            url1 = check_local_exist(clear_string(urls[0]))
            url2 = check_local_exist(clear_string(urls[1]))
            result = calc_similarity(url1=url1, url2=url2)
            results.append(result)
    return results


if __name__ == '__main__':
    simi_list = batch_process("data/train_urls")
    for simi in simi_list:
        print(simi)
